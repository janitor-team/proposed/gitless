Source: gitless
Section: devel
Priority: optional
Maintainer: Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>
Uploaders: Peter Pentchev <roam@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 dh-sequence-python3,
 git <!nocheck>,
 python3-all,
 python3-setuptools,
 python3-clint <!nocheck>,
 python3-pygit2 <!nocheck>,
 python3-sh (>= 1.11) <!nocheck>
Standards-Version: 4.5.0
Homepage: https://gitless.com/
Vcs-Git: https://salsa.debian.org/python-team/applications/gitless.git
Vcs-Browser: https://salsa.debian.org/python-team/applications/gitless
Rules-Requires-Root: no

Package: gitless
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
 git,
 python3-pkg-resources
Recommends: less
Multi-Arch: foreign
Description: version control system on top of Git
 Gitless is an experimental version control system built on top of Git.
 Many people complain that Git is hard to use.  The Gitless authors think
 the problem lies deeper than the user interface, in the concepts
 underlying Git.  Gitless is an experiment to see what happens if
 a simple veneer is put on an app that changes the underlying concepts.
 Because Gitless is implemented on top of Git (could be considered what
 Git pros call a "porcelain" of Git), you can always fall back on Git.
 And of course your coworkers you share a repo with need never know that
 you're not a Git aficionado.
